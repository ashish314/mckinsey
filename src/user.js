class User {
  constructor (name = 'bob', isEmployee = false, isAffiliate = false, isPriviledgeCustomer = false) {
    this.name = name
    this.isEmployee = isEmployee
    this.isAffiliate = isAffiliate
    this.isPriviledgeCustomer = isPriviledgeCustomer
  }
}

module.exports = User
