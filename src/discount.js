const { userModel, itemModel } = require('./dbConnection')

const calculatePricedDiscount = (billAmount) => {
  if (billAmount / 100 >= 1) {
    return billAmount / 100 * 5
  }
  return 0
}

const calculatePercentageDiscount = (user, items) => {
  let totalBillingAmount = 0
  let billingAmountExcludingGroceries = 0

  items.forEach((item) => {
    totalBillingAmount = totalBillingAmount + item.price
    if (!item.isGrocery) {
      billingAmountExcludingGroceries = billingAmountExcludingGroceries + item.price
    }
  })
  if (user.isEmployee) {
    return billingAmountExcludingGroceries * (3 / 10)
  }

  if (user.isAffiliate) {
    return billingAmountExcludingGroceries * (1 / 10)
  }

  if (user.isPriviledgeCustomer) {
    return billingAmountExcludingGroceries * (5 / 100)
  }
}

const calculateDiscount = async (userId, itemIds) => {
  const user = await userModel.findOne({ where: { id: userId } })
  const itemsArray = await itemModel.findAll({ where: { id: itemIds } })

  let actualAmount = 0
  itemsArray.forEach((item) => {
    actualAmount = actualAmount + item.price
  })
  const percenatgeDiscount = calculatePercentageDiscount(user, itemsArray)
  const pricedDiscount = calculatePricedDiscount(actualAmount - percenatgeDiscount)
  return {
    actualAmount: actualAmount.toFixed(2),
    discount: (percenatgeDiscount + pricedDiscount).toFixed(2),
    amountToBePaid: (actualAmount - (percenatgeDiscount + pricedDiscount)).toFixed(2)
  }
}

module.exports = calculateDiscount
