var Sequelize = require('sequelize')

// Setting up the config
var sequelize = new Sequelize('mckinsey', 'root', 'root', {
  host: 'localhost',
  port: '8889',
  dialect: 'mysql'
})

const userModel = require('./models/user')(sequelize, Sequelize)
const itemModel = require('./models/item')(sequelize, Sequelize)

// async function populateDb () {
//   await userModel.create({ name: 'bob', isEmployee: true, isAffiliate: false, isPriviledgeCustomer: false })
//   await itemModel.create({ name: 'item1', isGrocery: false, price: 60 })
// }

sequelize.sync()
  .then(() => {
    console.log('tables created')
  })

// populateDb();

module.exports = {
  userModel,
  itemModel
}
