module.exports = (sequelize, Sequelize) => {
  return sequelize.define('item', {
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    isGrocery: Sequelize.BOOLEAN,
    price: Sequelize.STRING
  })
}
