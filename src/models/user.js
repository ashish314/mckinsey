module.exports = (sequelize, Sequelize) => {
  return sequelize.define('user', {
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    isEmployee: Sequelize.BOOLEAN,
    isAffiliate: Sequelize.BOOLEAN,
    isPriviledgeCustomer: Sequelize.BOOLEAN
  })
}
