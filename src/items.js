class Item {
  constructor (name = 'item', price = 60, isGrocery = false) {
    this.name = name
    this.price = price
    this.isGrocery = isGrocery
  }
}

module.exports = Item
