// log setup
const winston = require('winston')

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [new winston.transports.Console()]
})

logger.info('This will not be logged in console transport because warn is set!')

logger.info('This will be logged in now!')

logger.level = 'debug'

logger.info('This will not be logged in now!')

logger.debug('This will be logged in now!')

module.exports = logger
