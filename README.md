# How to install

1. install npm and node (node version 11.12.0).

2. run git clone repo.

3. run cd mckinsey and then run npm install.


# install global dependencies for command line utility.

1. we use esLint in entire project to maintain code standard, to use it      via commandline we need to install it globally via 
   "npm install eslint -g" command

   Now eslint can be used to check check errors on a single as
   eslint "filePath"
   or
   eslint "directoryPath/" for entire directory.

2. Code coverage:
   For this run "npm install nyc -g" and then from project root run
   npm test.

# How to run tests:

  run "npm install mocha -g" and then from project root run
  
  "mocha"