/* eslint-disable no-undef */
const assert = require('assert')
const calculateDiscount = require('./../src/discount')
const sinon = require('sinon')
const { userModel, itemModel } = require('./../src/dbConnection')
var sandbox = require('sinon').createSandbox()

describe('begin', function () {
  afterEach(function () {
    sandbox.restore()
  })
  describe('tests', function () {
    it('employee should get only 30% discount on all non grocery items if price is below 100', async function () {
      const userQueryStub = sandbox.stub(userModel, 'findOne').resolves({ id: '123', name: 'bob', isEmployee: true })
      const itemQueryStub = sandbox.stub(itemModel, 'findAll').resolves([
        { id: '1', name: 'item1', price: 60, isGrocery: false }
      ])
      const discount = await calculateDiscount('1', ['1'])
      sinon.assert.calledOnce(userQueryStub)
      sinon.assert.calledOnce(itemQueryStub)
      assert.equal(discount.actualAmount, 60.00)
      assert.equal(discount.discount, 18.00)
      assert.equal(discount.amountToBePaid, 42.00)
    })

    it('employee should get 30% discount on all non grocery items and additional discount on total price if price exceeds 100', async function () {
      const userQueryStub = sandbox.stub(userModel, 'findOne').resolves({ id: '123', name: 'bob', isEmployee: true })
      const itemQueryStub = sandbox.stub(itemModel, 'findAll').resolves([
        { id: '1', name: 'item1', price: 60, isGrocery: false },
        { id: '2', name: 'item2', price: 60, isGrocery: false },
        { id: '3', name: 'item3', price: 50, isGrocery: true }
      ])
      const discount = await calculateDiscount('1', ['1', '2', '3'])
      sinon.assert.calledOnce(userQueryStub)
      sinon.assert.calledOnce(itemQueryStub)
      assert.equal(discount.actualAmount, 170.00)
      assert.equal(discount.discount, 42.70)
      assert.equal(discount.amountToBePaid, 127.30)
    })

    it('should get 30% discount if user is both employee and affiliate', async function () {
      const userQueryStub = sandbox.stub(userModel, 'findOne').resolves({ id: '123', name: 'bob', isEmployee: true, isAffiliate: true })
      const itemQueryStub = sandbox.stub(itemModel, 'findAll').resolves([
        { id: '1', name: 'item1', price: 60, isGrocery: false }
      ])
      const discount = await calculateDiscount('1', ['1'])
      sinon.assert.calledOnce(userQueryStub)
      sinon.assert.calledOnce(itemQueryStub)
      assert.equal(discount.actualAmount, 60.00)
      assert.equal(discount.discount, 18.00)
      assert.equal(discount.amountToBePaid, 42.00)
    })

    it('affiliate user should get 10% discount', async function () {
      const userQueryStub = sandbox.stub(userModel, 'findOne').resolves({ id: '123', name: 'bob', isEmployee: false, isAffiliate: true })
      const itemQueryStub = sandbox.stub(itemModel, 'findAll').resolves([
        { id: '1', name: 'item1', price: 60, isGrocery: false }
      ])
      const discount = await calculateDiscount('1', ['1'])
      sinon.assert.calledOnce(userQueryStub)
      sinon.assert.calledOnce(itemQueryStub)
      assert.equal(discount.actualAmount, 60.00)
      assert.equal(discount.discount, 6.00)
      assert.equal(discount.amountToBePaid, 54.00)
    })

    it('priviledge user should get 5% discount', async function () {
      const userQueryStub = sandbox.stub(userModel, 'findOne').resolves({ id: '123', name: 'bob', isEmployee: false, isPriviledgeCustomer: true })
      const itemQueryStub = sandbox.stub(itemModel, 'findAll').resolves([
        { id: '1', name: 'item1', price: 60, isGrocery: false }
      ])
      const discount = await calculateDiscount('1', ['1'])
      sinon.assert.calledOnce(userQueryStub)
      sinon.assert.calledOnce(itemQueryStub)
      assert.equal(discount.actualAmount, 60.00)
      assert.equal(discount.discount, 3.00)
      assert.equal(discount.amountToBePaid, 57.00)
    })

    it('affiliate user should get 10% discount and additional discount if price exceeds 100', async function () {
      const userQueryStub = sandbox.stub(userModel, 'findOne').resolves({ id: '123', name: 'bob', isEmployee: false, isAffiliate: true })
      const itemQueryStub = sandbox.stub(itemModel, 'findAll').resolves([
        { id: '1', name: 'item1', price: 60, isGrocery: false },
        { id: '2', name: 'item2', price: 60, isGrocery: false },
        { id: '3', name: 'item3', price: 50, isGrocery: true }
      ])
      const discount = await calculateDiscount('1', ['1', '2', '3'])
      sinon.assert.calledOnce(userQueryStub)
      sinon.assert.calledOnce(itemQueryStub)
      assert.equal(discount.actualAmount, 170.00)
      assert.equal(discount.discount, 19.90)
    })

    it('percentage based discounts should not be applied to grocery items', async function () {
      const userQueryStub = sandbox.stub(userModel, 'findOne').resolves({ id: '123', name: 'bob', isEmployee: false, isAffiliate: true })
      const itemQueryStub = sandbox.stub(itemModel, 'findAll').resolves([
        { id: '2', name: 'item2', price: 50, isGrocery: true },
        { id: '3', name: 'item3', price: 50, isGrocery: true }
      ])
      const discount = await calculateDiscount('1', ['2', '3'])
      sinon.assert.calledOnce(userQueryStub)
      sinon.assert.calledOnce(itemQueryStub)
      assert.equal(discount.actualAmount, 100.00)
      assert.equal(discount.discount, 5.00)
    })

    it('should be able to handle discount when no item is purchased', async function () {
      const userQueryStub = sandbox.stub(userModel, 'findOne').resolves({ id: '123', name: 'bob', isEmployee: false, isAffiliate: true })
      const itemQueryStub = sandbox.stub(itemModel, 'findAll').resolves([])
      const discount = await calculateDiscount('1', [])
      sinon.assert.calledOnce(userQueryStub)
      sinon.assert.calledOnce(itemQueryStub)
      assert.equal(discount.actualAmount, 0.00)
      assert.equal(discount.discount, 0.00)
    })
  })
})
